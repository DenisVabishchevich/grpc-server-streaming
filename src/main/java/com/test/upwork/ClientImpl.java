package com.test.upwork;

import com.test.upwork.protobuf.PayloadServiceGrpc;
import com.test.upwork.protobuf.Req;
import com.test.upwork.protobuf.Res;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.io.IOException;
import java.util.Iterator;

public class ClientImpl {

    public static void main(String[] args) throws IOException {

        String url;

        if (args.length == 0) {
            System.out.println("Please set URL as first parameter");
            System.out.println("Using https://www.tut.by");
            url = "https://www.tut.by";
        } else {
            url = args[0];
        }

        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", 54321)
                .usePlaintext()
                .maxInboundMessageSize(2048)
                .build();

        PayloadServiceGrpc.PayloadServiceBlockingStub stub = PayloadServiceGrpc.newBlockingStub(channel);

        Iterator<Res.Response> payloads = stub.getPayload(Req.Request.newBuilder()
                .setUrl(url)
                .build());

        payloads.forEachRemaining(e -> System.out.println(e.getPayload()));

        channel.shutdown();
    }

}
