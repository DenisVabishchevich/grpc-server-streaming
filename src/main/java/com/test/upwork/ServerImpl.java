package com.test.upwork;

import com.test.upwork.protobuf.PayloadServiceGrpc;
import com.test.upwork.protobuf.Req;
import com.test.upwork.protobuf.Res;
import io.grpc.Server;
import io.grpc.netty.NettyServerBuilder;
import io.grpc.stub.StreamObserver;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServerImpl extends PayloadServiceGrpc.PayloadServiceImplBase {

    private static Server server;

    public static void main(String[] args) throws IOException, InterruptedException {

        server = NettyServerBuilder
                .forPort(54321)
                .addService(new ServerImpl()).build();

        server.start();
        server.awaitTermination();
    }

    @Override
    public void getPayload(Req.Request request, StreamObserver<Res.Response> responseObserver) {

        BufferedReader in = null;

        try {
            URL url = new URL(request.getUrl());
            char[] byff = new char[1024];

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            // headers
            Res.Response response = Res.Response.newBuilder()
                    .setPayload(con.getHeaderFields().toString())
                    .build();
            responseObserver.onNext(response);

            in = new BufferedReader(new InputStreamReader(con.getInputStream()));

            // payload
            while (in.read(byff, 0, 1024) >= 0) {
                String payload = new String(byff);
                response = Res.Response.newBuilder()
                        .setPayload(payload)
                        .build();
                responseObserver.onNext(response);
            }

            responseObserver.onCompleted();

        } catch (
                Exception e) {
            e.printStackTrace();
        } finally {
            close(in);
            server.shutdown();
        }

    }

    private void close(BufferedReader in) {
        try {
            if (in != null) {
                in.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
